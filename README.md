=======================
ZendSkeletonApplication
=======================

Найти доку по:

- Zend\Mvc\Application
- Zend\Mvc\Controller\AbstractActionController

- Zend\View\Model\ViewModel

- Zend\Authentication\AuthenticationService
- Zend\Authentication\Adapter\DbTable

- Zend\Db\TableGateway\TableGateway
- Zend\Db\ResultSet\ResultSet
- Zend\Db\Adapter\Adapter

- Zend\InputFilter\InputFilter

- Zend\Form\Form



Ссылки:

    - Украинское сообщество Zend Framework 2
    http://zf2.com.ua/doc

    - Пример разработки блога на Zend Framework 2
    http://habrahabr.ru/post/192522/
    http://habrahabr.ru/post/192608/
    http://habrahabr.ru/post/192726/



==========================
My SQL-script
==========================

CREATE DATABASE zf2;
CREATE TABLE user (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    name TEXT NOT NULL,
    email VARCHAR(255) NOT NULL,
    password TEXT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX idx_email(email)
);

==============
My 'local.php'
==============

return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=zf2;host=localhost',
        'username' => 'root',
        'password' => '',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);